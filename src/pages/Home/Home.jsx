import {
  Button,
  ButtonBase,
  makeStyles,
  CircularProgress,
  Box,
  Divider,
  useTheme,
  useMediaQuery,
} from '@material-ui/core';
import { Grid, Typography } from '@material-ui/core';
import Layout from '../../components/Layout/Layout';
import {
  StyledActivePrograms,
  StyledImg,
  StyledCheck,
  StyledDivCheck,
  StyledTime,
  StyledOverplayCheck,
} from './Home.styles';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import CheckIcon from '@material-ui/icons/Check';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import { TableContainer } from '@material-ui/core';
import { Table } from '@material-ui/core';
import { TableHead } from '@material-ui/core';
import { TableRow } from '@material-ui/core';
import { TableCell } from '@material-ui/core';
import { TableBody } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  image: {
    width: 68,
    height: 68,
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  },
  customBgTypo: {
    backgroundColor: '#5855d6',
    color: 'white',
    textAlign: 'center',
    borderRadius: 25,
    fontSize: 11,
    fontWeight: 600,
  },
  customTypo: {
    color: '#9592A6',
    textAlign: 'center',
  },
}));

const Home = () => {
  const classes = useStyles();
  const theme = useTheme();
  const deviceLg = useMediaQuery(theme.breakpoints.up('lg'));
  return (
    <Layout>
      <Grid container spacing={5}>
        <Grid item md={12} lg={9} container spacing={1}>
          <Grid item xs={12}>
            <Typography
              variant='h6'
              component='h6'
              style={{ fontWeight: 'bold', float: 'left', marginBottom: 10 }}
            >
              Hello, Mia!
            </Typography>
            {deviceLg && (
              <Button
                style={{
                  float: 'right',
                  color: '#5855D6',
                  textTransform: 'unset',
                }}
              >
                <span style={{ marginRight: 7 }}>
                  <svg
                    width='17'
                    height='17'
                    viewBox='0 0 17 17'
                    fill='none'
                    xmlns='http://www.w3.org/2000/svg'
                  >
                    <path
                      d='M7.08333 9.20833H2.83333C2.64547 9.20833 2.4653 9.28296 2.33247 9.4158C2.19963 9.54864 2.125 9.7288 2.125 9.91667V14.1667C2.125 14.3545 2.19963 14.5347 2.33247 14.6675C2.4653 14.8004 2.64547 14.875 2.83333 14.875H7.08333C7.27119 14.875 7.45136 14.8004 7.5842 14.6675C7.71704 14.5347 7.79167 14.3545 7.79167 14.1667V9.91667C7.79167 9.7288 7.71704 9.54864 7.5842 9.4158C7.45136 9.28296 7.27119 9.20833 7.08333 9.20833ZM6.375 13.4583H3.54167V10.625H6.375V13.4583ZM14.1667 2.125H9.91667C9.7288 2.125 9.54864 2.19963 9.4158 2.33247C9.28296 2.4653 9.20833 2.64547 9.20833 2.83333V7.08333C9.20833 7.27119 9.28296 7.45136 9.4158 7.5842C9.54864 7.71704 9.7288 7.79167 9.91667 7.79167H14.1667C14.3545 7.79167 14.5347 7.71704 14.6675 7.5842C14.8004 7.45136 14.875 7.27119 14.875 7.08333V2.83333C14.875 2.64547 14.8004 2.4653 14.6675 2.33247C14.5347 2.19963 14.3545 2.125 14.1667 2.125ZM13.4583 6.375H10.625V3.54167H13.4583V6.375ZM14.1667 11.3333H12.75V9.91667C12.75 9.7288 12.6754 9.54864 12.5425 9.4158C12.4097 9.28296 12.2295 9.20833 12.0417 9.20833C11.8538 9.20833 11.6736 9.28296 11.5408 9.4158C11.408 9.54864 11.3333 9.7288 11.3333 9.91667V11.3333H9.91667C9.7288 11.3333 9.54864 11.408 9.4158 11.5408C9.28296 11.6736 9.20833 11.8538 9.20833 12.0417C9.20833 12.2295 9.28296 12.4097 9.4158 12.5425C9.54864 12.6754 9.7288 12.75 9.91667 12.75H11.3333V14.1667C11.3333 14.3545 11.408 14.5347 11.5408 14.6675C11.6736 14.8004 11.8538 14.875 12.0417 14.875C12.2295 14.875 12.4097 14.8004 12.5425 14.6675C12.6754 14.5347 12.75 14.3545 12.75 14.1667V12.75H14.1667C14.3545 12.75 14.5347 12.6754 14.6675 12.5425C14.8004 12.4097 14.875 12.2295 14.875 12.0417C14.875 11.8538 14.8004 11.6736 14.6675 11.5408C14.5347 11.408 14.3545 11.3333 14.1667 11.3333ZM7.08333 2.125H2.83333C2.64547 2.125 2.4653 2.19963 2.33247 2.33247C2.19963 2.4653 2.125 2.64547 2.125 2.83333V7.08333C2.125 7.27119 2.19963 7.45136 2.33247 7.5842C2.4653 7.71704 2.64547 7.79167 2.83333 7.79167H7.08333C7.27119 7.79167 7.45136 7.71704 7.5842 7.5842C7.71704 7.45136 7.79167 7.27119 7.79167 7.08333V2.83333C7.79167 2.64547 7.71704 2.4653 7.5842 2.33247C7.45136 2.19963 7.27119 2.125 7.08333 2.125ZM6.375 6.375H3.54167V3.54167H6.375V6.375Z'
                      fill='#5855D6'
                    />
                  </svg>
                </span>
                <p style={{ marginTop: 0, marginBottom: 10 }}>Customize</p>
              </Button>
            )}
          </Grid>
          <Grid item xs={12}>
            <StyledActivePrograms
              container
              spacing={2}
              style={{ marginBottom: 25 }}
              direction='column'
            >
              <Grid item xs={12}>
                <Typography
                  variant='body1'
                  component='p'
                  style={{ float: 'left', fontWeight: 500 }}
                >
                  Active Programs
                </Typography>
                <Button
                  style={{
                    float: 'right',
                  }}
                >
                  <MoreHorizIcon htmlColor='#9592A6' />
                </Button>
              </Grid>
              <Grid item xs={12} container>
                <Grid item xs={deviceLg ? 3 : 9}>
                  <Grid xs={12} sm container spacing={2}>
                    <Grid item>
                      <ButtonBase className={classes.image}>
                        <img
                          className={classes.img}
                          alt='complex'
                          src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/master/src/common/img/img2.png'
                        />
                      </ButtonBase>
                    </Grid>
                    <Grid item xs container direction='column' spacing={2}>
                      <Grid item xs>
                        <Typography
                          gutterBottom
                          className={classes.customBgTypo}
                          variant={deviceLg ? 'subtitle1' : 'body2'}
                        >
                          Master of Computer Science
                        </Typography>
                        <Typography
                          variant={deviceLg ? 'body2' : 'caption'}
                          style={{ fontWeight: 600 }}
                        >
                          University of Upstate at Brookstone
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                {deviceLg ? (
                  <>
                    <Grid item xs={1} />
                    <Grid item xs={8} container spacing={3}>
                      <Grid item xs={3}>
                        <Typography
                          variant='h5'
                          className={classes.customTypo}
                          style={{ fontSize: 30, fontWeight: 'bold' }}
                        >
                          1
                        </Typography>
                        <Typography
                          variant='body2'
                          className={classes.customTypo}
                        >
                          Overdue <br /> Assignments
                        </Typography>
                      </Grid>
                      <Grid item xs={3}>
                        <Typography
                          variant='h5'
                          className={classes.customTypo}
                          style={{ fontSize: 30, fontWeight: 'bold' }}
                        >
                          5
                        </Typography>
                        <Typography
                          variant='body2'
                          className={classes.customTypo}
                        >
                          Overdue <br /> Assignments
                        </Typography>
                      </Grid>
                      <Grid item xs={3}>
                        <Typography
                          variant='h5'
                          className={classes.customTypo}
                          style={{ fontSize: 30, fontWeight: 'bold' }}
                        >
                          A-
                        </Typography>
                        <Typography
                          variant='body2'
                          className={classes.customTypo}
                        >
                          Overdue <br /> Assignments
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        xs={3}
                        style={{ display: 'flex', flexDirection: 'column' }}
                      >
                        <div style={{ margin: '0 auto' }}>
                          <Box position='relative' display='inline-flex'>
                            <CircularProgress
                              variant='determinate'
                              value={45}
                              thickness={8}
                              size={50}
                            />
                            <Box
                              top={0}
                              left={0}
                              bottom={0}
                              right={0}
                              position='absolute'
                              display='flex'
                              alignItems='center'
                              justifyContent='center'
                            >
                              <Typography
                                variant='caption'
                                component='div'
                                color='textSecondary'
                              >{`${Math.round(45)}%`}</Typography>
                            </Box>
                          </Box>
                        </div>
                        <Typography
                          variant='body2'
                          className={classes.customTypo}
                        >
                          Completion
                        </Typography>
                      </Grid>
                    </Grid>
                  </>
                ) : (
                  <Grid
                    item
                    xs={3}
                    style={{ display: 'flex', flexDirection: 'column' }}
                  >
                    <div style={{ margin: '0 auto' }}>
                      <Box position='relative' display='inline-flex'>
                        <CircularProgress
                          variant='determinate'
                          value={45}
                          thickness={8}
                          size={50}
                        />
                        <Box
                          top={0}
                          left={0}
                          bottom={0}
                          right={0}
                          position='absolute'
                          display='flex'
                          alignItems='center'
                          justifyContent='center'
                        >
                          <Typography
                            variant='caption'
                            component='div'
                            color='textSecondary'
                          >{`${Math.round(45)}%`}</Typography>
                        </Box>
                      </Box>
                    </div>
                    <Typography variant='body2' className={classes.customTypo}>
                      Completion
                    </Typography>
                  </Grid>
                )}
              </Grid>
              <Divider style={{ margin: '15px 0' }} />
              <Grid item xs={12} container>
                <Grid item xs={deviceLg ? 3 : 9}>
                  <Grid item xs={12} sm container spacing={2}>
                    <Grid item>
                      <ButtonBase className={classes.image}>
                        <img
                          className={classes.img}
                          alt='complex'
                          src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d6dbc27e416dce23969ad97ffe7550654a229c7d/src/common/img/img1.png'
                        />
                      </ButtonBase>
                    </Grid>
                    <Grid item xs container direction='column' spacing={2}>
                      <Grid item xs>
                        <Typography
                          gutterBottom
                          className={classes.customBgTypo}
                          variant={deviceLg ? 'subtitle1' : 'body2'}
                        >
                          CERTIFICATE UX/UI design
                        </Typography>
                        <Typography
                          variant={deviceLg ? 'body2' : 'caption'}
                          component='p'
                          style={{
                            fontWeight: 600,
                          }}
                        >
                          Academy of Art and Technology at Dursburg
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                {deviceLg ? (
                  <>
                    <Grid item xs={1} />
                    <Grid item xs={8} container spacing={3}>
                      <Grid item xs={3}>
                        <Typography
                          variant='h5'
                          className={classes.customTypo}
                          style={{ fontSize: 30, fontWeight: 'bold' }}
                        >
                          0
                        </Typography>
                        <Typography
                          variant='body2'
                          className={classes.customTypo}
                        >
                          Overdue <br /> Assignments
                        </Typography>
                      </Grid>
                      <Grid item xs={3}>
                        <Typography
                          variant='h5'
                          className={classes.customTypo}
                          style={{ fontSize: 30, fontWeight: 'bold' }}
                        >
                          6
                        </Typography>
                        <Typography
                          variant='body2'
                          className={classes.customTypo}
                        >
                          Overdue <br /> Assignments
                        </Typography>
                      </Grid>
                      <Grid item xs={3}>
                        <Typography
                          variant='h5'
                          className={classes.customTypo}
                          style={{ fontSize: 30, fontWeight: 'bold' }}
                        >
                          --
                        </Typography>
                        <Typography
                          variant='body2'
                          className={classes.customTypo}
                        >
                          Overdue <br /> Assignments
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        xs={3}
                        style={{ display: 'flex', flexDirection: 'column' }}
                      >
                        <div style={{ margin: '0 auto' }}>
                          <Box position='relative' display='inline-flex'>
                            <CircularProgress
                              variant='determinate'
                              value={25}
                              thickness={8}
                              size={50}
                            />
                            <Box
                              top={0}
                              left={0}
                              bottom={0}
                              right={0}
                              position='absolute'
                              display='flex'
                              alignItems='center'
                              justifyContent='center'
                            >
                              <Typography
                                variant='caption'
                                component='div'
                                color='textSecondary'
                              >{`${Math.round(25)}%`}</Typography>
                            </Box>
                          </Box>
                        </div>
                        <Typography
                          variant='body2'
                          className={classes.customTypo}
                        >
                          Completion
                        </Typography>
                      </Grid>
                    </Grid>
                  </>
                ) : (
                  <Grid
                    item
                    xs={3}
                    style={{ display: 'flex', flexDirection: 'column' }}
                  >
                    <div style={{ margin: '0 auto' }}>
                      <Box position='relative' display='inline-flex'>
                        <CircularProgress
                          variant='determinate'
                          value={25}
                          thickness={8}
                          size={50}
                        />
                        <Box
                          top={0}
                          left={0}
                          bottom={0}
                          right={0}
                          position='absolute'
                          display='flex'
                          alignItems='center'
                          justifyContent='center'
                        >
                          <Typography
                            variant='caption'
                            component='div'
                            color='textSecondary'
                          >{`${Math.round(25)}%`}</Typography>
                        </Box>
                      </Box>
                    </div>
                    <Typography variant='body2' className={classes.customTypo}>
                      Completion
                    </Typography>
                  </Grid>
                )}
              </Grid>
            </StyledActivePrograms>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={6}>
              <Grid item xs={12} lg={5}>
                {deviceLg ? (
                  <StyledActivePrograms>
                    <div style={{ position: 'relative' }}>
                      <StyledImg
                        src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/b729c1b99d27997f8f6fe411c46d844bda5e0977/src/common/img/Rectangle%2026.png'
                        alt=''
                      />
                      <StyledCheck>
                        <div>
                          <CheckIcon htmlColor='#4ED964' />
                        </div>
                        <Typography
                          variant='body2'
                          style={{
                            fontSize: 8,
                            textAlign: 'center',
                            color: 'white',
                          }}
                        >
                          Checked In
                        </Typography>
                      </StyledCheck>
                      <StyledDivCheck>
                        <Grid container>
                          <Grid item xs={9}>
                            <Typography
                              variant='h5'
                              style={{ color: '#0E1566' }}
                            >
                              UX Research - Week 3
                            </Typography>
                            <Typography
                              variant='body2'
                              style={{ color: '#9592A6' }}
                            >
                              CERTIFICATE UX/UI design
                            </Typography>
                            <Button
                              style={{
                                color: '#FF2C55',
                                textTransform: 'unset',
                              }}
                            >
                              <AccessTimeIcon />
                              <p style={{ margin: 0, marginLeft: 4 }}>
                                In Progress
                              </p>
                            </Button>
                          </Grid>
                          <Grid item xs={3}>
                            <Typography
                              variant='h6'
                              style={{
                                textAlign: 'center',
                                color: '#9592A6',
                                fontWeight: 'bold',
                              }}
                            >
                              08
                            </Typography>
                            <Typography
                              variant='subtitle2'
                              style={{
                                textAlign: 'center',
                                color: '#9592A6',
                                fontWeight: 'bold',
                              }}
                            >
                              APRIL
                            </Typography>
                            <Button
                              variant='text'
                              style={{
                                color: '#5855D6',
                                textTransform: 'none',
                                fontWeight: 600,
                              }}
                            >
                              Join Now
                            </Button>
                          </Grid>
                        </Grid>
                      </StyledDivCheck>
                    </div>
                  </StyledActivePrograms>
                ) : (
                  <StyledActivePrograms spacing={2} container>
                    <Grid item xs={4} style={{ margin: 'auto' }}>
                      <Grid item>
                        <div style={{ position: 'relative' }}>
                          <ButtonBase
                            className={classes.image}
                            style={{
                              width: '100%',
                              height: 'auto',
                              borderRadius: 5,
                            }}
                          >
                            <img
                              className={classes.img}
                              alt='complex'
                              src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/b729c1b99d27997f8f6fe411c46d844bda5e0977/src/common/img/Rectangle%2026.png'
                            />
                          </ButtonBase>
                          <StyledOverplayCheck>
                            <div>
                              <CheckIcon htmlColor='#4ED964' />
                            </div>
                            <Typography
                              variant='body2'
                              style={{
                                fontSize: 8,
                                textAlign: 'center',
                                color: 'white',
                              }}
                            >
                              Checked In
                            </Typography>
                          </StyledOverplayCheck>
                        </div>
                      </Grid>
                    </Grid>
                    <Grid item xs={8}>
                      <Grid container direction='row'>
                        <Grid item>
                          <Grid container>
                            <Grid item xs={8}>
                              <Typography
                                variant='h6'
                                style={{ color: '#0E1566' }}
                              >
                                UX Research - Week 3
                              </Typography>
                              <Typography
                                variant='caption'
                                style={{ color: '#9592A6' }}
                              >
                                CERTIFICATE UX/UI design
                              </Typography>
                            </Grid>
                            <Grid item xs={4}>
                              <Typography
                                variant='h6'
                                style={{
                                  textAlign: 'right',
                                  color: '#9592A6',
                                  fontWeight: 'bold',
                                }}
                              >
                                08
                              </Typography>
                              <Typography
                                variant='subtitle2'
                                style={{
                                  textAlign: 'right',
                                  color: '#9592A6',
                                  fontWeight: 'bold',
                                }}
                              >
                                APRIL
                              </Typography>
                            </Grid>
                          </Grid>
                        </Grid>
                        <Grid item style={{ width: '100%' }}>
                          <Grid container>
                            <Grid item xs={8}>
                              <Button
                                style={{
                                  color: '#FF2C55',
                                  textTransform: 'unset',
                                }}
                              >
                                <AccessTimeIcon />
                                <p style={{ margin: 0, marginLeft: 4 }}>
                                  In Progress
                                </p>
                              </Button>
                            </Grid>
                            <Grid item xs={4} justify='flex-end'>
                              <Button
                                variant='text'
                                style={{
                                  color: '#5855D6',
                                  textTransform: 'none',
                                  fontWeight: 600,
                                  whiteSpace: 'nowrap',
                                }}
                              >
                                Join Now
                              </Button>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </StyledActivePrograms>
                )}
              </Grid>
              <Grid item xs={12} lg={7} direction='column'>
                <Grid item xs={12}>
                  <StyledActivePrograms spacing={2} direction='column'>
                    <Typography
                      variant='body1'
                      component='p'
                      style={{ float: 'left', fontWeight: 500 }}
                    >
                      Upcoming Sessions
                    </Typography>
                    <Button
                      style={{
                        float: 'right',
                      }}
                    >
                      <MoreHorizIcon htmlColor='#9592A6' />
                    </Button>
                    <TableContainer>
                      <Table>
                        <TableHead>
                          <TableRow>
                            <TableCell style={{ color: '#65708D' }}>
                              Date
                            </TableCell>
                            <TableCell style={{ color: '#65708D' }}>
                              Class
                            </TableCell>
                            <TableCell style={{ color: '#65708D' }}>
                              Time
                            </TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          <TableRow hover={true}>
                            <TableCell component='th' scope='row'>
                              <Typography
                                variant='body2'
                                style={{
                                  textAlign: 'center',
                                  color: '#5855D6',
                                  fontWeight: 'bold',
                                }}
                              >
                                Tomorrow
                              </Typography>
                              <Typography
                                variant='h6'
                                style={{
                                  textAlign: 'center',
                                  color: '#9592A6',
                                  fontWeight: 'bold',
                                }}
                              >
                                09
                              </Typography>
                              <Typography
                                variant='body2'
                                style={{
                                  textAlign: 'center',
                                  color: '#9592A6',
                                  fontWeight: 'bold',
                                }}
                              >
                                APRIL
                              </Typography>
                            </TableCell>
                            <TableCell>
                              <Typography
                                variant='body1'
                                style={{ color: '#0E1566' }}
                              >
                                Database and Information Systems
                              </Typography>
                              <Typography
                                variant='body2'
                                style={{ color: '#9592A6' }}
                              >
                                Master of Computer Science
                              </Typography>
                            </TableCell>
                            <TableCell>
                              <StyledTime>
                                <Typography
                                  variant='body1'
                                  style={{
                                    color: '#333269',
                                    textAlign: 'center',
                                    fontWeight: 'bold',
                                  }}
                                >
                                  8:30am-1:00pm
                                </Typography>
                              </StyledTime>
                            </TableCell>
                          </TableRow>
                          <TableRow hover={true}>
                            <TableCell component='th' scope='row'>
                              <Typography
                                variant='body2'
                                style={{
                                  textAlign: 'center',
                                  color: '#5855D6',
                                  fontWeight: 'bold',
                                }}
                              >
                                Wed
                              </Typography>
                              <Typography
                                variant='h6'
                                style={{
                                  textAlign: 'center',
                                  color: '#9592A6',
                                  fontWeight: 'bold',
                                }}
                              >
                                10
                              </Typography>
                              <Typography
                                variant='body2'
                                style={{
                                  textAlign: 'center',
                                  color: '#9592A6',
                                  fontWeight: 'bold',
                                }}
                              >
                                APRIL
                              </Typography>
                            </TableCell>
                            <TableCell>
                              <Typography
                                variant='body1'
                                style={{ color: '#0E1566' }}
                              >
                                AI and Machine Learning
                              </Typography>
                              <Typography
                                variant='body2'
                                style={{ color: '#9592A6' }}
                              >
                                Master of Computer Science
                              </Typography>
                            </TableCell>
                            <TableCell>
                              <StyledTime>
                                <Typography
                                  variant='body1'
                                  style={{
                                    color: '#333269',
                                    textAlign: 'center',
                                    fontWeight: 'bold',
                                  }}
                                >
                                  9:30am-12:00pm
                                </Typography>
                              </StyledTime>
                            </TableCell>
                          </TableRow>
                          <TableRow hover={true}>
                            <TableCell component='th' scope='row'>
                              <Typography
                                variant='body2'
                                style={{
                                  textAlign: 'center',
                                  color: '#5855D6',
                                  fontWeight: 'bold',
                                }}
                              >
                                Web
                              </Typography>
                              <Typography
                                variant='h6'
                                style={{
                                  textAlign: 'center',
                                  color: '#9592A6',
                                  fontWeight: 'bold',
                                }}
                              >
                                10
                              </Typography>
                              <Typography
                                variant='body2'
                                style={{
                                  textAlign: 'center',
                                  color: '#9592A6',
                                  fontWeight: 'bold',
                                }}
                              >
                                APRIL
                              </Typography>
                            </TableCell>
                            <TableCell>
                              <Typography
                                variant='body1'
                                style={{ color: '#0E1566' }}
                              >
                                Definition & Ideation
                              </Typography>
                              <Typography
                                variant='body2'
                                style={{ color: '#9592A6' }}
                              >
                                Certificate UX/UI Design
                              </Typography>
                            </TableCell>
                            <TableCell>
                              <StyledTime>
                                <Typography
                                  variant='body1'
                                  style={{
                                    color: '#333269',
                                    textAlign: 'center',
                                    fontWeight: 'bold',
                                  }}
                                >
                                  3:30pm-7:00pm
                                </Typography>
                              </StyledTime>
                            </TableCell>
                          </TableRow>
                        </TableBody>
                      </Table>
                    </TableContainer>
                    <div
                      style={{ display: 'flex', justifyContent: 'flex-end' }}
                    >
                      <Button
                        variant='text'
                        style={{
                          color: '#5855D6',
                          textTransform: 'none',
                          fontWeight: 600,
                          marginTop: 25,
                        }}
                      >
                        See All Sessions
                      </Button>
                    </div>
                  </StyledActivePrograms>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Layout>
  );
};

export default Home;
