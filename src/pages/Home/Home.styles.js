import { Grid } from '@material-ui/core';
import styled from 'styled-components';
import { device } from '../../common/device';

export const StyledActivePrograms = styled(Grid)`
  border-radius: 20px;
  background-color: white;
  padding: 10px;
  @media ${device.tablet} {
    padding: 25px;
  }
`;

export const StyledImg = styled.img`
  border-radius: 10px;
  width: 100%;
  height: auto;
  object-fit: cover;
`;

export const StyledCheck = styled.div`
  background-color: #5855d6;
  display: flex;
  flex-direction: column;
  position: absolute;
  align-items: center;
  border-radius: 0px 0px 10px 10px;
  top: 0;
  right: 20px;
  padding: 10px;
`;
export const StyledOverplayCheck = styled.div`
  background-color: rgba(88, 85, 214, 0.75);
  display: flex;
  flex-direction: column;
  position: absolute;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  border-radius: 5px;
  z-index: 2;
  top: 0;
`;

export const StyledDivCheck = styled.div`
  position: absolute;
  width: 105%;
  background: #fafaff;
  box-shadow: 0px 14px 20px rgba(0, 0, 0, 0.1);
  border-radius: 10px;
  padding: 20px;
  bottom: -100px;
  left: -10px;
`;

export const StyledTime = styled.div`
  background: #e7e7ff;
  border-radius: 11px;
  padding: 3px 10px;
`;
