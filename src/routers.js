import { Redirect, Route, Switch } from 'react-router-dom';
import App from './App';
import Home from './pages/Home/Home';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';

const Routers = () => {
  return (
    <Switch>
      <Route exact path='/' component={App} />
      <Route path='/sign-up' component={SignUp} />
      <Route path='/sign-in' component={SignIn} />
      <Route path='/home' component={Home} />
      <Redirect to='/' />
    </Switch>
  );
};

export default Routers;
