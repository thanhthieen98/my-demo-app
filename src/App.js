import { Container, List, ListItem, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import './App.css';
import ButtonGradient from './components/Button/ButtonGradient';

const useStyles = makeStyles((theme) => ({
  nav: {
    width: '100%',
    marginTop: theme.spacing(3),
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    marginTop: theme.spacing(20),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}));

function App() {
  const classes = useStyles();
  return (
    <Container component='main' maxWidth='xs'>
      <div className={classes.paper}>
        <Typography component='h1' variant='h5'>
          Demo App
        </Typography>
        <List component='nav' className={classes.nav}>
          <ListItem component={Link} to='/sign-up'>
            <ButtonGradient content='Sign Up' styles={{ padding: 30 }} />
          </ListItem>
          <ListItem component={Link} to='/sign-in'>
            <ButtonGradient content='Sign In' styles={{ padding: 30 }} />
          </ListItem>
        </List>
      </div>
    </Container>
  );
}

export default App;
