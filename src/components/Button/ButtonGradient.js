import { Button, StylesProvider } from '@material-ui/core';
import './Styles.css';

const ButtonGradient = ({ content, styles, className, type = 'button' }) => {
  return (
    <StylesProvider injectFirst>
      <Button
        fullWidth
        className={`btn__gradient ${className}`}
        style={styles}
        type={type}
      >
        {content}
      </Button>
    </StylesProvider>
  );
};

export default ButtonGradient;
