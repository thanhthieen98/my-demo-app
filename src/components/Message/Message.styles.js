import { TableCell, TableContainer } from '@material-ui/core';
import styled from 'styled-components';
import { device } from '../../common/device';

export const Main = styled.div`
  position: fixed;
  bottom: 0;
  right: 0;
  overflow-y: scroll;
  @media ${device.tablet} {
    right: 2%;
    width: 325px;
    height: unset;
    overflow-y: unset;
  }
  width: 100%;
  height: 100%;
  z-index: 2;
`;

export const StyledMessage = styled.div`
  background-color: #fafaff;
  border-radius: 20px 20px 0px 0px;
  padding: 18px;
  width: 100%;
  border-bottom: 1px #eeeeee solid;
  outline: none;
  transition: 0.4s;
  :hover {
    background-color: #ccc;
  }
`;

export const StyledPanel = styled.div`
  padding: 20px 18px;
  display: ${(props) => (props.active ? 'block' : 'none')};
  background-color: white;
`;

export const StyledStory = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 15px;
  padding-bottom: 5px;
  max-width: 100%;
  overflow-x: scroll;
  &::-webkit-scrollbar {
    height: 6px;
  }
  &:hover {
    &::-webkit-scrollbar-thumb {
      background: #9592a6;
      border-radius: 10px;
      display: block;
    }
  }
  &::-webkit-scrollbar-thumb {
    display: none;
  }
  &::-webkit-scrollbar-track {
    display: none;
  }
`;

export const StyledItemStory = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 20px;
  :hover {
    cursor: pointer;
  }
`;

export const StyledTextStory = styled.p`
  color: #77838f;
  text-align: center;
  font-size: 12px;
  margin: 0;
`;

export const StyledAvatarStory = styled.div`
  border: 2px solid;
  border-color: #ff2c55;
  border-radius: 50%;
`;

export const StyledNameInb = styled.p`
  font-weight: 600;
  font-size: 13px;
`;
export const StyledContentInb = styled.p`
  font-weight: ${(props) => (props.seen ? 'normal' : 600)};
  font-size: 13px;
  color: ${(props) => props.seen && '#9592a6'};
`;
export const StyledTimeInb = styled.p`
  font-size: 10px;
  color: #77838f;
  text-align: right;
  white-space: nowrap;
`;
export const StyledCountInb = styled.div`
  background: #5855d6;
  border-radius: 50%;
  color: white;
  text-align: center;
  margin-left: 20px;
`;

export const StyledTable = styled(TableContainer)`
  max-height: 670px;
  @media ${device.tablet} {
    max-height: unset;
  }
  &::-webkit-scrollbar {
    width: 6px;
  }
  &:hover {
    &::-webkit-scrollbar-thumb {
      background: #9592a6;
      border-radius: 10px;
      display: block;
    }
  }
  &::-webkit-scrollbar-thumb {
    display: none;
  }
  &::-webkit-scrollbar-track {
    display: none;
  }
`;

export const StyledTableCell = styled(TableCell)`
  cursor: pointer;
  padding: 6px 6px 6px 12px;
`;
