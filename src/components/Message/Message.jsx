import {
  Avatar,
  Badge,
  Divider,
  Grid,
  IconButton,
  Table,
  TableBody,
  TableRow,
  useMediaQuery,
} from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import CreateIcon from '@material-ui/icons/Create';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import SearchIcon from '@material-ui/icons/Search';
import React, { useState } from 'react';
import {
  Main,
  StyledAvatarStory,
  StyledContentInb,
  StyledCountInb,
  StyledItemStory,
  StyledMessage,
  StyledNameInb,
  StyledPanel,
  StyledStory,
  StyledTable,
  StyledTableCell,
  StyledTextStory,
  StyledTimeInb,
} from './Message.styles';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  customBadge: {
    backgroundColor: '#6FCF97',
    color: 'white',
  },
}));

const MessageComp = () => {
  const [active, setActive] = useState();
  const classes = useStyles();
  const theme = useTheme();
  const deviceLg = useMediaQuery(theme.breakpoints.up('lg'));

  return (
    <Main>
      <StyledMessage>
        <Grid container spacing={4} alignItems='center'>
          <Grid item lg={2}>
            <Badge
              classes={{ badge: classes.customBadge }}
              overlap='circle'
              badgeContent=' '
              variant='dot'
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
            >
              <Avatar
                alt='Remy Sharp'
                src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/fb1efe6483c9a8dd9d61598f5d1154d96bba6d33/src/common/avatar/user.png?width=1'
              />
            </Badge>
          </Grid>
          <Grid item lg={4}>
            <Typography variant='body1' component='p'>
              Messaging
            </Typography>
          </Grid>
          <Grid item lg={1}>
            <IconButton>
              <SearchIcon htmlColor='#9592A6' />
            </IconButton>
          </Grid>
          <Grid item lg={1}>
            <IconButton>
              <CreateIcon htmlColor='#9592A6' />
            </IconButton>
          </Grid>
          <Grid item lg={1}>
            <IconButton>
              <MoreHorizIcon htmlColor='#9592A6' />
            </IconButton>
          </Grid>
          <Grid item lg={2} alignContent='flex-end'>
            {!active ? (
              <IconButton onClick={() => setActive(!active)}>
                <ExpandLessIcon htmlColor='#5855D6' />
              </IconButton>
            ) : (
              <IconButton onClick={() => setActive(!active)}>
                <ExpandMoreIcon htmlColor='#5855D6' />
              </IconButton>
            )}
          </Grid>
        </Grid>
      </StyledMessage>
      <StyledPanel active={active}>
        <StyledStory>
          <StyledItemStory>
            <Avatar>
              <AddIcon />
            </Avatar>
            <StyledTextStory>Add</StyledTextStory>
          </StyledItemStory>
          <StyledItemStory>
            <StyledAvatarStory>
              <Avatar
                alt='Remy Sharp'
                src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d3f19694502f3bb93c9cad68c7f5304e43c8ddfa/src/common/avatar/Image.png?width=1'
              />
            </StyledAvatarStory>
            <StyledTextStory>Dianne</StyledTextStory>
          </StyledItemStory>
          <StyledItemStory>
            <StyledAvatarStory>
              <Avatar
                alt='Remy Sharp'
                src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d3f19694502f3bb93c9cad68c7f5304e43c8ddfa/src/common/avatar/Image%20(1).png?width=1'
              />
            </StyledAvatarStory>
            <StyledTextStory>Jarvis</StyledTextStory>
          </StyledItemStory>
          <StyledItemStory>
            <StyledAvatarStory>
              <Avatar
                alt='Remy Sharp'
                src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d3f19694502f3bb93c9cad68c7f5304e43c8ddfa/src/common/avatar/Image%20(2).png?width=1'
              />
            </StyledAvatarStory>
            <StyledTextStory>Miles</StyledTextStory>
          </StyledItemStory>
          <StyledItemStory>
            <StyledAvatarStory>
              <Avatar
                alt='Remy Sharp'
                src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d3f19694502f3bb93c9cad68c7f5304e43c8ddfa/src/common/avatar/Image%20(2).png?width=1'
              />
            </StyledAvatarStory>
            <StyledTextStory>Miles</StyledTextStory>
          </StyledItemStory>
          <StyledItemStory>
            <StyledAvatarStory>
              <Avatar
                alt='Remy Sharp'
                src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d3f19694502f3bb93c9cad68c7f5304e43c8ddfa/src/common/avatar/Image%20(2).png?width=1'
              />
            </StyledAvatarStory>
            <StyledTextStory>Miles</StyledTextStory>
          </StyledItemStory>
          <StyledItemStory>
            <StyledAvatarStory>
              <Avatar
                alt='Remy Sharp'
                src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d3f19694502f3bb93c9cad68c7f5304e43c8ddfa/src/common/avatar/Image%20(2).png?width=1'
              />
            </StyledAvatarStory>
            <StyledTextStory>Miles</StyledTextStory>
          </StyledItemStory>
          <StyledItemStory>
            <StyledAvatarStory>
              <Avatar
                alt='Remy Sharp'
                src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d3f19694502f3bb93c9cad68c7f5304e43c8ddfa/src/common/avatar/Image%20(2).png?width=1'
              />
            </StyledAvatarStory>
            <StyledTextStory>Miles</StyledTextStory>
          </StyledItemStory>
        </StyledStory>
        <Divider />
        <StyledTable>
          <Table size='small'>
            <TableBody>
              <TableRow hover={true}>
                <StyledTableCell component='th' scope='row'>
                  <Avatar
                    alt='Remy Sharp'
                    src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d3f19694502f3bb93c9cad68c7f5304e43c8ddfa/src/common/avatar/Mask.png?width=1'
                  />
                </StyledTableCell>
                <StyledTableCell>
                  <StyledNameInb>Sophie Kowalski</StyledNameInb>
                  <StyledContentInb>
                    Thanks, Mia. Please let me know when I can...
                  </StyledContentInb>
                </StyledTableCell>
                <StyledTableCell>
                  <StyledTimeInb>3 min</StyledTimeInb>
                  <StyledCountInb>1</StyledCountInb>
                </StyledTableCell>
              </TableRow>
              <TableRow hover={true}>
                <StyledTableCell component='th' scope='row'>
                  <Avatar
                    alt='Remy Sharp'
                    src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d3f19694502f3bb93c9cad68c7f5304e43c8ddfa/src/common/avatar/Image%20(3).png?width=1'
                  />
                </StyledTableCell>
                <StyledTableCell>
                  <StyledNameInb>John Kumoz</StyledNameInb>
                  <StyledContentInb seen={true}>
                    Hey Mia. Do you know where I can find last...
                  </StyledContentInb>
                </StyledTableCell>
                <StyledTableCell>
                  <StyledTimeInb>45 min</StyledTimeInb>
                </StyledTableCell>
              </TableRow>
              <TableRow hover={true}>
                <StyledTableCell component='th' scope='row'>
                  <Avatar
                    alt='Remy Sharp'
                    src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d3f19694502f3bb93c9cad68c7f5304e43c8ddfa/src/common/avatar/Image%20(4).png?width=1'
                  />
                </StyledTableCell>
                <StyledTableCell>
                  <StyledNameInb>Chris Meinfield</StyledNameInb>
                  <StyledContentInb seen={true}>
                    OK. I’ll have her give you a call and you can figure...
                  </StyledContentInb>
                </StyledTableCell>
                <StyledTableCell>
                  <StyledTimeInb>Yesterday</StyledTimeInb>
                </StyledTableCell>
              </TableRow>
              <TableRow hover={true}>
                <StyledTableCell component='th' scope='row'>
                  <Avatar
                    alt='Remy Sharp'
                    src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d3f19694502f3bb93c9cad68c7f5304e43c8ddfa/src/common/avatar/Image%20(5).png?width=1'
                  />
                </StyledTableCell>
                <StyledTableCell>
                  <StyledNameInb>Zoe Acorn</StyledNameInb>
                  <StyledContentInb>
                    Any chance we can do it later this week? I’m out...
                  </StyledContentInb>
                </StyledTableCell>
                <StyledTableCell>
                  <StyledTimeInb>Monday</StyledTimeInb>
                  <StyledCountInb>2</StyledCountInb>
                </StyledTableCell>
              </TableRow>
              <TableRow hover={true}>
                <StyledTableCell component='th' scope='row'>
                  <Avatar
                    alt='Remy Sharp'
                    src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d3f19694502f3bb93c9cad68c7f5304e43c8ddfa/src/common/avatar/Mask%20(1).png?width=1'
                  />
                </StyledTableCell>
                <StyledTableCell>
                  <StyledNameInb>Johm Kan</StyledNameInb>
                  <StyledContentInb seen={true}>
                    Sure, no problem. I’ll send it over once we complete...
                  </StyledContentInb>
                </StyledTableCell>
                <StyledTableCell>
                  <StyledTimeInb>Tuesday</StyledTimeInb>
                </StyledTableCell>
              </TableRow>
              <TableRow hover={true}>
                <StyledTableCell component='th' scope='row'>
                  <Avatar
                    alt='Remy Sharp'
                    src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/d3f19694502f3bb93c9cad68c7f5304e43c8ddfa/src/common/avatar/Mask%20(1).png?width=1'
                  />
                </StyledTableCell>
                <StyledTableCell>
                  <StyledNameInb>Johm Kan</StyledNameInb>
                  <StyledContentInb seen={true}>
                    Sure, no problem. I’ll send it over once we complete...
                  </StyledContentInb>
                </StyledTableCell>
                <StyledTableCell>
                  <StyledTimeInb>Tuesday</StyledTimeInb>
                </StyledTableCell>
              </TableRow>
            </TableBody>
          </Table>
        </StyledTable>
      </StyledPanel>
    </Main>
  );
};
export default MessageComp;
