import { Drawer, ListItem } from '@material-ui/core';
import styled from 'styled-components';
import { device } from '../../common/device';

const drawerWidth = 240;
const styles = (theme) => ({
  appBar: {
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflow: 'hidden',
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    [theme.breakpoints.down('md')]: {
      position: 'fixed',
      top: 0,
      right: 0,
      zIndex: 99999,
    },
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  paper: {
    background: '#333269',
  },
});

export const Main = styled.div`
  display: flex;
  color: #333269;
`;
export const ListItemCustom = styled(ListItem)`
  padding: 0px 0px 0px 25px;
  margin: 20px 0px;
  position: relative;
  ::before {
    position: absolute;
    content: '';
    left: 0;
    top: 1px;
    background: ${(props) => props.active && '#ee3637'};
    width: 5px;
    height: 34px;
    border-radius: 5px;
  }
`;

export const StyledChevronLeftIcon = styled.div`
  position: absolute;
  top: 0px;
  right: 0px;
  display: ${(props) => (props.open ? 'block' : 'none')};
`;

export const StyledIconButton = styled.div`
  display: flex;
  align-items: center;
  position: static;
  justify-content: center;
  margin: 40px 0px;
`;

export const StyledDrawer = styled(Drawer)`
  ${({ theme, open }) => {
    const classes = styles(theme);
    return {
      ...classes.drawer,
      ...(open ? classes.drawerOpen : classes.drawerClose),
    };
  }}

  .MuiDrawer-paper {
    ${({ theme, open }) => {
      const classes = styles(theme);
      return {
        ...classes.paper,
        ...(open ? classes.drawerOpen : classes.drawerClose),
      };
    }}

    &::-webkit-scrollbar {
      width: 2px;
    }
    &:hover {
      &::-webkit-scrollbar-thumb {
        display: none;
      }
    }
    &::-webkit-scrollbar-thumb {
      display: none;
    }
    &::-webkit-scrollbar-track {
      display: none;
    }
  }
`;

export const StyledAppBar = styled.div`
  ${({ theme, open }) => {
    const classes = styles(theme);
    return {
      ...classes.appBar,
      ...(open && classes.appBarShift),
    };
  }}
`;

export const Container = styled.div`
  position: relative;
  margin: 20px;
  @media ${device.tablet} {
    margin: 45px 50px;
  }
`;
export const Overplay = styled.div`
  position: fixed;
  display: ${(props) => (props.open ? 'block' : 'none')};
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  overflow: hidden;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 10000;
`;
