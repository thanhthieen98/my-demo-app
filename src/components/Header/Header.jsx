import {
  Avatar,
  Badge,
  Grid,
  IconButton,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MoreIcon from '@material-ui/icons/MoreVert';
import NotificationsIcon from '@material-ui/icons/Notifications';
import SearchIcon from '@material-ui/icons/Search';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import {
  AppBarCustom,
  Circle,
  GridCustom,
  InputCustom,
  Search,
  SearchIconDiv,
  User,
  UserInfo,
} from './Header.styles';
import { Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: 'flex',
  },
  customBadge: {
    backgroundColor: '#6FCF97',
    color: 'white',
  },
}));

const Header = ({ setOpen, open }) => {
  const classes = useStyles();
  const theme = useTheme();
  const deviceLg = useMediaQuery(theme.breakpoints.up('lg'));

  return (
    <div className={classes.root}>
      {!deviceLg && (
        <Button
          style={{
            padding: 0,
            background: '#333269',
            width: 60,
            display: 'flex',
            justifyContent: 'center',
            zIndex: 1100,
          }}
          onClick={() => setOpen(!open)}
        >
          <div>
            <svg
              width='28'
              height='37'
              viewBox='0 0 28 37'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M23.144 7.24539C22.0651 7.24662 21.0306 7.6755 20.2679 8.43795C19.5049 9.2004 19.0758 10.2341 19.0748 11.3123V21.4422C19.0862 22.1672 18.9533 22.887 18.6836 23.56C18.4139 24.233 18.013 24.8456 17.5041 25.3623C16.9952 25.879 16.3885 26.2893 15.7194 26.5693C15.0502 26.8494 14.332 26.9936 13.6066 26.9936C12.8812 26.9936 12.163 26.8494 11.4938 26.5693C10.8247 26.2893 10.218 25.879 9.70914 25.3623C9.20023 24.8456 8.79929 24.233 8.52963 23.56C8.25997 22.887 8.12699 22.1672 8.13844 21.4422V11.3123C8.13844 10.2338 7.70971 9.19949 6.94656 8.43687C6.18341 7.67425 5.14835 7.24582 4.0691 7.24582C2.98985 7.24582 1.95479 7.67425 1.19164 8.43687C0.428486 9.19949 -0.000244141 10.2338 -0.000244141 11.3123V22.5148C-0.000244194 24.3004 0.351705 26.0686 1.03551 27.7184C1.71932 29.3681 2.7216 30.867 3.98511 32.1297C5.24861 33.3923 6.74863 34.3939 8.39948 35.0773C10.0503 35.7605 11.8197 36.1122 13.6066 36.1122C15.3935 36.1122 17.1629 35.7605 18.8138 35.0773C20.4646 34.3939 21.9646 33.3923 23.228 32.1297C24.4916 30.867 25.4939 29.3681 26.1776 27.7184C26.8615 26.0686 27.2134 24.3004 27.2134 22.5148V11.3123C27.2122 10.2341 26.7833 9.20038 26.0204 8.43795C25.2574 7.6755 24.223 7.24662 23.144 7.24539ZM25.4331 22.514C25.4331 25.6485 24.187 28.6544 21.9692 30.8707C19.7513 33.087 16.7432 34.3323 13.6066 34.3323C10.47 34.3323 7.46191 33.087 5.24401 30.8707C3.02612 28.6544 1.78011 25.6485 1.78011 22.514V11.3123C1.78532 10.7091 2.02876 10.1323 2.45748 9.70761C2.88618 9.28288 3.46541 9.04458 4.0691 9.04458C4.67278 9.04458 5.25202 9.28288 5.68072 9.70761C6.10942 10.1323 6.35288 10.7091 6.35809 11.3123V21.4422C6.35809 23.3634 7.12177 25.2058 8.48112 26.5642C9.84048 27.9227 11.6842 28.6857 13.6066 28.6857C15.529 28.6857 17.3728 27.9227 18.7321 26.5642C20.0913 25.2058 20.8551 23.3634 20.8551 21.4422V11.3123C20.8551 10.7057 21.0962 10.1239 21.5255 9.69491C21.9548 9.26593 22.537 9.02494 23.144 9.02494C23.7512 9.02494 24.3334 9.26593 24.7627 9.69491C25.1918 10.1239 25.4331 10.7057 25.4331 11.3123V22.514Z'
                fill='url(#paint0_linear)'
              />
              <path
                d='M12.7165 9.3187V21.0462C12.7165 21.2822 12.8102 21.5085 12.9772 21.6753C13.1441 21.8422 13.3705 21.9358 13.6066 21.9358C13.8427 21.9358 14.0691 21.8422 14.2361 21.6753C14.403 21.5085 14.4968 21.2822 14.4968 21.0462V9.3187C15.6461 9.09741 16.6717 8.45598 17.3731 7.51967C18.0746 6.58334 18.4017 5.41937 18.2904 4.25501C18.1792 3.09065 17.6377 2.00954 16.7716 1.22275C15.9055 0.435959 14.7771 0 13.6066 0C12.4362 0 11.3077 0.435959 10.4417 1.22275C9.57559 2.00954 9.03407 3.09065 8.92286 4.25501C8.81162 5.41937 9.13868 6.58334 9.84016 7.51967C10.5416 8.45598 11.5671 9.09741 12.7165 9.3187ZM13.6066 1.78103C14.1851 1.78103 14.7506 1.95246 15.2316 2.27363C15.7126 2.59481 16.0875 3.0513 16.3089 3.5854C16.5303 4.11949 16.5882 4.70719 16.4753 5.27418C16.3624 5.84117 16.0838 6.36196 15.6748 6.77073C15.2657 7.17949 14.7445 7.45785 14.1771 7.57061C13.6098 7.68338 13.0217 7.62546 12.4872 7.40422C11.9528 7.18296 11.496 6.80831 11.1746 6.32762C10.8532 5.84694 10.6817 5.28182 10.6817 4.70372C10.6826 3.92883 10.9911 3.18592 11.5394 2.638C12.0878 2.09009 12.8312 1.78188 13.6066 1.78103Z'
                fill='url(#paint1_linear)'
              />
              <defs>
                <linearGradient
                  id='paint0_linear'
                  x1='-0.000244141'
                  y1='21.6789'
                  x2='27.2134'
                  y2='21.6789'
                  gradientUnits='userSpaceOnUse'
                >
                  <stop stop-color='#EC441E' />
                  <stop offset='1' stop-color='#EE2A4A' />
                </linearGradient>
                <linearGradient
                  id='paint1_linear'
                  x1='8.90154'
                  y1='10.9679'
                  x2='18.3117'
                  y2='10.9679'
                  gradientUnits='userSpaceOnUse'
                >
                  <stop stop-color='#EC441E' />
                  <stop offset='1' stop-color='#EE2A4A' />
                </linearGradient>
              </defs>
            </svg>
          </div>
        </Button>
      )}
      <AppBarCustom position='static'>
        <Toolbar style={{ margin: deviceLg && '15px 30px' }}>
          <Search>
            <SearchIconDiv>
              <SearchIcon style={{ color: '#5855D6' }} />
            </SearchIconDiv>
            <InputCustom placeholder='Search' />
          </Search>
          <div
            style={{
              ...(deviceLg
                ? { margin: '0 35px' }
                : { width: 'fit-content', paddingLeft: 5 }),
            }}
          >
            <GridCustom container spacing={deviceLg ? 5 : 1} justify='center'>
              <Grid item>
                <div style={{ display: 'flex' }}>
                  <Circle style={{ marginRight: 13 }}>
                    <Badge
                      classes={{ badge: classes.customBadge }}
                      overlap='circle'
                      badgeContent=' '
                      variant='dot'
                    >
                      <svg
                        width='23'
                        height='23'
                        viewBox='0 0 18 18'
                        fill='none'
                        xmlns='http://www.w3.org/2000/svg'
                      >
                        <path
                          d='M14.3025 3.69752C13.0719 2.45889 11.4478 1.68843 9.70999 1.51895C7.97223 1.34947 6.22979 1.7916 4.78307 2.76912C3.33634 3.74663 2.276 5.19827 1.78485 6.87378C1.2937 8.54928 1.40251 10.3436 2.09253 11.9475C2.16445 12.0966 2.18804 12.2644 2.16003 12.4275L1.50003 15.6C1.4746 15.7217 1.47979 15.8477 1.51514 15.9668C1.55048 16.086 1.61488 16.1944 1.70253 16.2825C1.77438 16.3538 1.85993 16.4099 1.95403 16.4473C2.04812 16.4847 2.14881 16.5026 2.25003 16.5H2.40003L5.61003 15.855C5.77318 15.8354 5.93862 15.8587 6.09003 15.9225C7.6939 16.6125 9.48827 16.7213 11.1638 16.2302C12.8393 15.739 14.2909 14.6787 15.2684 13.232C16.2459 11.7853 16.6881 10.0428 16.5186 8.30505C16.3491 6.56729 15.5787 4.94311 14.34 3.71252L14.3025 3.69752ZM6.00003 9.75002C5.85169 9.75002 5.70669 9.70603 5.58335 9.62362C5.46001 9.54121 5.36388 9.42407 5.30712 9.28703C5.25035 9.14999 5.2355 8.99919 5.26444 8.8537C5.29338 8.70821 5.36481 8.57458 5.4697 8.46969C5.57459 8.3648 5.70822 8.29337 5.85371 8.26443C5.9992 8.23549 6.15 8.25034 6.28704 8.30711C6.42409 8.36387 6.54122 8.46 6.62363 8.58334C6.70604 8.70668 6.75003 8.85168 6.75003 9.00002C6.75003 9.19893 6.67101 9.38969 6.53036 9.53035C6.38971 9.671 6.19894 9.75002 6.00003 9.75002ZM9.00003 9.75002C8.85169 9.75002 8.70669 9.70603 8.58335 9.62362C8.46001 9.54121 8.36388 9.42407 8.30712 9.28703C8.25035 9.14999 8.2355 8.99919 8.26444 8.8537C8.29338 8.70821 8.36481 8.57458 8.4697 8.46969C8.57459 8.3648 8.70822 8.29337 8.85371 8.26443C8.9992 8.23549 9.15 8.25034 9.28704 8.30711C9.42409 8.36387 9.54122 8.46 9.62363 8.58334C9.70604 8.70668 9.75003 8.85168 9.75003 9.00002C9.75003 9.19893 9.67101 9.38969 9.53036 9.53035C9.38971 9.671 9.19894 9.75002 9.00003 9.75002ZM12 9.75002C11.8517 9.75002 11.7067 9.70603 11.5833 9.62362C11.46 9.54121 11.3639 9.42407 11.3071 9.28703C11.2504 9.14999 11.2355 8.99919 11.2644 8.8537C11.2934 8.70821 11.3648 8.57458 11.4697 8.46969C11.5746 8.3648 11.7082 8.29337 11.8537 8.26443C11.9992 8.23549 12.15 8.25034 12.287 8.30711C12.4241 8.36387 12.5412 8.46 12.6236 8.58334C12.706 8.70668 12.75 8.85168 12.75 9.00002C12.75 9.19893 12.671 9.38969 12.5304 9.53035C12.3897 9.671 12.1989 9.75002 12 9.75002Z'
                          fill='white'
                        />
                      </svg>
                    </Badge>
                  </Circle>
                  {deviceLg && (
                    <Circle>
                      <Badge
                        color='error'
                        overlap='circle'
                        badgeContent=' '
                        variant='dot'
                      >
                        <NotificationsIcon />
                      </Badge>
                    </Circle>
                  )}
                </div>
              </Grid>
              <Grid item>
                <User>
                  <div>
                    <Avatar
                      alt='Remy Sharp'
                      src='https://gitlab.com/thanhthieen98/my-demo-app/-/raw/fb1efe6483c9a8dd9d61598f5d1154d96bba6d33/src/common/avatar/user.png?width=1'
                    />
                  </div>
                  {deviceLg && (
                    <UserInfo>
                      <Typography
                        variant='subtitle2'
                        style={{ color: '#0A043C', fontWeight: 'bold' }}
                      >
                        Mia V
                      </Typography>
                      <Typography
                        variant='subtitle2'
                        style={{ color: '#0A043C' }}
                      >
                        Student
                      </Typography>
                    </UserInfo>
                  )}
                </User>
              </Grid>
            </GridCustom>
          </div>
          {deviceLg && (
            <IconButton
              aria-label='display more actions'
              edge='end'
              color='inherit'
            >
              <MoreIcon htmlColor='#9592A6' />
            </IconButton>
          )}
        </Toolbar>
      </AppBarCustom>
    </div>
  );
};

export default Header;
