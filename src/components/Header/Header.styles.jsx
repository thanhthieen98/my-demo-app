import { AppBar, Grid, InputBase } from '@material-ui/core';
import { createMuiTheme } from '@material-ui/core/styles';
import styled from 'styled-components';
import { device } from '../../common/device';

const theme = createMuiTheme();
export const AppBarCustom = styled(AppBar)`
  background-color: white;
  box-shadow: unset;
  ${(props) => props.theme.breakpoints.down('md')} {
    flex-shrink: unset;
  }
`;

export const Search = styled.div`
  position: relative;
  flex-grow: 1;
  margin-right: 0px;
  margin-left: 0px;
  width: 100%;
  display: flex;
  height: 42px;
  background-color: #f8f8fc;
  border-radius: 5px;
  @media ${device.tablet} {
    border-right: 1px rgba(149, 146, 166, 0.75) solid;
    background-color: unset;
    border-radius: unset;
    margin-right: 16px;
  }
`;

export const SearchIconDiv = styled.div`
  height: 100%;
  position: absolute;
  pointer-events: none;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const InputCustom = styled(InputBase)`
  padding-left: calc(${theme.spacing(5)}px);
  width: 100%;
`;

export const Circle = styled.div`
  background-color: #5855d6;
  width: 38px;
  height: 38px;
  border-radius: 50%;
  filter: drop-shadow(2px 3px 10px rgba(0, 0, 0, 0.25));
  display: flex;
  justify-content: center;
  align-items: center;
  :hover {
    cursor: pointer;
  }
`;

export const User = styled.div`
  display: flex;
  :hover {
    cursor: pointer;
  }
`;

export const UserInfo = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 1rem;
`;

export const GridCustom = styled(Grid)`
  flex-wrap: nowrap;
`;
